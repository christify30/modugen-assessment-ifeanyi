const ValidateFile = (object: any): boolean => {
  if (!Array.isArray(object)) return false;// ensure its array
  if (object.length < 1) return false; // ensure that it has contents
  for (let i = 0; i < object.length; i += 1) {
    if (!object[i].bounding_points || !Array.isArray(object[i].bounding_points)) return false;
    // ensure that the bounding box is array
    const obj = object[i].bounding_points;
    if (!Array.isArray(obj)) return false;
    for (let l = 0; l < obj.length; l += 1) {
      const checkKeysOfPoints: Array<string> = Object.keys(obj[l]);
      if (checkKeysOfPoints.length < 3) return false;// ensure it has 3 points
      for (let k = 0; k < checkKeysOfPoints.length; k += 1) {
        const cordinates = checkKeysOfPoints[k].toLowerCase();
        if (cordinates !== 'x' && cordinates !== 'y' && cordinates !== 'z') return false;
        // ensure that all cordinates are correct
      }
    }
  }
  return true;
};

export default ValidateFile;
