import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import CreateSceneType from 'models/types/CreateScene';
import SceneInterface from 'models/interface/Scene';
import cameraType from 'models/types/CameraType';
// import cameraType from 'models/enum/Cameras';
import changeCameraProjectionType from 'models/types/changeCameraProjection';

class Scene implements SceneInterface {
    private element: any;

    private width: number;// = window.innerWidth;

    private height: number;// = window.innerHeight;

    private camera: cameraType;

    private renderer: THREE.WebGLRenderer;

    public constructor(element: any, windowWidth: number, windowHeight: number) {
      this.element = element;
      this.width = windowWidth;
      this.height = windowHeight;
      this.camera = this.perspectiveCamera();
      this.camera.position.set(0, 0, 100);
      this.camera.lookAt(0, 0, 0);
      this.renderer = new THREE.WebGLRenderer();
    }

    createScene(): CreateSceneType {
      const scene = new THREE.Scene();
      const { camera } = this;
      const { renderer } = this;
      renderer.setSize(window.innerWidth, window.innerHeight);
      const controls = new OrbitControls(this.camera, renderer.domElement);
      this.element.current.appendChild(renderer.domElement);
      return {
        scene, controls, renderer, camera,
      };
    }

    changeCameraProjection(typeOfCamera: string): changeCameraProjectionType {
      let { camera } = this;
      let theControls: OrbitControls = new OrbitControls(camera, this.renderer.domElement);
      if (typeOfCamera === 'perspective') {
        camera = this.perspectiveCamera();
        camera.position.set(0, 0, 100);
        camera.lookAt(0, 0, 0);
        theControls = new OrbitControls(camera, this.renderer.domElement);
      } else {
        camera = this.orthographicCamera();
        camera.position.set(0, 0, 20);
        camera.lookAt(1, 0, 1);
        camera.zoom = this.width / 50;
        theControls = new OrbitControls(camera, this.renderer.domElement);
      }
      return { camera, controls: theControls };
    }

    private perspectiveCamera(): THREE.PerspectiveCamera {
      return new THREE.PerspectiveCamera(45, this.width / this.height, 1, 500);
    }

    private orthographicCamera() {
      return new THREE.OrthographicCamera(this.width / -2,
        this.width / 2, this.height / 2, this.height / -2, 1, 1000);
    }
}

export default Scene;
