import Polygon from 'models/interface/Polygon';
import * as THREE from 'three';

export type pointType = Array<THREE.Vector3 | THREE.Vector2>
export type objectType = {
    myPoint: pointType
}
const GeneratePoints = (data: Array<Polygon>) => {
  // console.log(data);
  const objects: Array<objectType> = [];
  for (let i: number = 0; i < data.length; i += 1) {
    objects.push({ myPoint: [] });
    const boundingBox = data[i].bounding_points;
    for (let j = 0; j < boundingBox.length; j += 1) {
      const xyz = boundingBox[j];
      objects[i].myPoint.push(new THREE.Vector3(xyz.x, xyz.y, xyz.z));
    }
  }
  return objects;
};

export default GeneratePoints;
