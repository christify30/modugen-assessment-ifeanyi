import * as THREE from 'three';
import Points from 'models/types/Points';
// import CreateSceneType from 'models/types/CreateScene';
import DrawProps from 'models/types/DrawProps';

type drawType = {
    line: THREE.Line<THREE.BufferGeometry, THREE.LineBasicMaterial>,
    ourScene: THREE.Scene | undefined
}
const Draw = (props: DrawProps): drawType | undefined => {
  const {
    sceneProps, e, startPosition, startMove, enableDrag,
  } = props;
  const endPosition: Points = {
    x: 0,
    y: 0,
    z: 0,
  };
  endPosition.x = (e.clientX / window.innerWidth) * 2 - 1;
  endPosition.y = (e.clientY / window.innerHeight) * 2 + 1;

  const clientX = (endPosition.x - startPosition.x) * 5;
  const clientY = (endPosition.y - startPosition.y) * -5;

  if (startMove && enableDrag) {
    // console.log('good');

    const material = new THREE.LineBasicMaterial({ color: 'orange' });
    const points = [];

    points.push(new THREE.Vector3(clientX, 0, 0));
    points.push(new THREE.Vector3(clientX, clientY, 0));
    points.push(new THREE.Vector3(0, clientY, 0));
    points.push(new THREE.Vector3(0, 0, 0));
    points.push(new THREE.Vector3(clientX, 0, 0));
    const geometry = new THREE.BufferGeometry().setFromPoints(points);
    const line = new THREE.Line(geometry, material);
    // console.log(sceneProps);
    if (sceneProps?.scene !== undefined) {
      line.position.z = 5;
      line.userData = {
        move: true,
        name: `Shape${(Date.now() / 1000).toFixed(0)}`,
      };
      const ourScene = sceneProps?.scene;
      const lastChild = ourScene.children[ourScene.children.length - 1];
      if (lastChild.userData?.move) {
        ourScene.children.splice(ourScene.children.indexOf(lastChild), 1);
      }
      ourScene.add(line);
      return { ourScene, line };
    }
    return undefined;
  }
  return undefined;
};

export default Draw;
