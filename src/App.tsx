import React, { useEffect, useRef, useState } from 'react';
import logo from 'assests/images/logo.png';
import * as THREE from 'three';
import 'styles/App.css';
import CreateSceneType from 'models/types/CreateScene';
import Scene from 'objects/Scene';
import Cameratype from 'models/types/CameraType';
import Draw from 'utils/MouseDragDraw';
import Points from 'models/types/Points';
import ValidateJsonStructure from 'validation/ValidateFileFormat';
import GeneratePoints, { objectType } from 'utils/Generatepoints';

let startMove: boolean = false;
let enableDrag: boolean = false;
const startPosition: Points = {
  x: 0,
  y: 0,
  z: 0,
};
let thecamera: Cameratype;
type bufferMesh = THREE.Line<THREE.BufferGeometry, THREE.LineBasicMaterial>

function App() {
  const screenRenderer: any = useRef();
  const [sceneProps, setSceneProps] = useState<CreateSceneType>();
  const [newScene, setNewScene] = useState<Scene>();
  const [initialized, setInitialized] = useState<boolean>(false);
  const [currentCameraType, setCurrentCameraType] = useState<string>('perspective');
  const [error, setError] = useState<string>('');
  const [projctedRectangle, setProjectedRectangle] = useState<bufferMesh>();

  const animate = () => {
    requestAnimationFrame(animate);
    thecamera.updateProjectionMatrix();
    sceneProps?.controls.update();
    sceneProps?.renderer.render(sceneProps?.scene, thecamera);
  };

  const mouseDown = (e: any) => {
    startMove = true;
    startPosition.x = (e.clientX / window.innerWidth) * 2 - 1;
    startPosition.y = (e.clientY / window.innerHeight) * 2 + 1;
  };

  const mouseDrag = (e: any) => {
    if (sceneProps !== undefined) {
      const drawProps = {
        sceneProps, e, startPosition, startMove, enableDrag,
      };
      const SceneWithDrawnBlock = Draw(drawProps);
      if (SceneWithDrawnBlock !== undefined) {
        const { ourScene, line } = SceneWithDrawnBlock;
        if (ourScene === undefined) return;
        setSceneProps({ ...sceneProps, scene: ourScene });
        setProjectedRectangle(line);
      }
    }
  };

  const mouseUp = () => {
    startMove = false;
  };

  const changeCamera = (projection: string) => {
    if (sceneProps?.camera !== undefined && newScene !== undefined) {
      const { camera, controls } = newScene?.changeCameraProjection(projection);
      thecamera = camera;
      enableDrag = false;
      if (projection !== 'perspective') {
        enableDrag = true;
        controls.enabled = false;
      }
      setCurrentCameraType(projection);
      setSceneProps({ ...sceneProps, camera, controls });
    }
  };

  useEffect(() => {
    const theScene = new Scene(screenRenderer, window.innerWidth, window.innerHeight);
    setNewScene(theScene); // created instance of Scene object stored in state
    const {
      scene, controls, renderer, camera,
    } = theScene.createScene();
    thecamera = camera; /* assigns camera to a global variable outside
    the function for refrece throughout the program lifecycle */
    const axesHelper = new THREE.AxesHelper(3);
    scene.add(axesHelper);
    setSceneProps({
      scene, controls, renderer, camera,
    });
    setInitialized(true);
  }, []);

  useEffect(() => {
    animate();
  }, [sceneProps]);

  useEffect(() => {
    if (!initialized) return;
    const theScreen = screenRenderer.current;
    theScreen.addEventListener('mousedown', mouseDown);
    theScreen.addEventListener('mousemove', mouseDrag);
    theScreen.addEventListener('mouseup', mouseUp);
  }, [initialized]); /* create mouse event listener after
   initializing the scene and adding axis helper */

  const renderPolygons = (objectsVector: Array<objectType>) => {
    if (sceneProps === undefined) return;
    const theSceneToRenderPolygon = sceneProps?.scene;
    for (let i = 0; i < objectsVector.length; i += 1) {
      const object: Array<THREE.Vector2> | Array<THREE.Vector3> = objectsVector[i].myPoint as any;
      const geometry = new THREE.BufferGeometry().setFromPoints(object);
      const material = new THREE.LineBasicMaterial({ color: '#fff' });
      const line = new THREE.Line(geometry, material);
      line.position.z = 0;
      line.position.x = 0;
      line.position.y = 0;
      theSceneToRenderPolygon?.add(line);
    }
    setSceneProps({ ...sceneProps, scene: theSceneToRenderPolygon });
  };

  const fileUploaded = (event: any) => {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      setError('');
      try {
        const jsonObj = JSON.parse(e?.target?.result);
        const validationSuccess = ValidateJsonStructure(jsonObj);
        if (!validationSuccess) {
          setError('Invalid Json file Please check and try again!');
          return;
        }
        const polygonsToRender: Array<objectType> = GeneratePoints(jsonObj);
        renderPolygons(polygonsToRender);
      } catch (err) {
        setError('Invalid Json file Please check and try again!');
      }
    };
    reader.onerror = () => {
      setError('Invalid Json file Please check and try again!');
    };
    reader.readAsText(event.target?.files[0]);
  };

  const projectRectangle = () => {
    if (sceneProps?.scene !== undefined) {
      const myScene = sceneProps.scene;
      myScene.children.forEach((child) => {
        if (child?.userData?.move === true) {
          child.position.setZ(0);
        }
      });
      setSceneProps({ ...sceneProps, scene: myScene });
    }
  };

  const changeRectanglesZ = (value: number) => {
    if (projctedRectangle !== undefined) {
      projctedRectangle.position.z = value;
      if (sceneProps === undefined) return;
      const newSceneProps = sceneProps?.scene;
      newSceneProps.children.splice(newSceneProps.children.indexOf(newSceneProps), 1);
      newSceneProps.add(projctedRectangle);
      setSceneProps({ ...sceneProps, scene: newSceneProps });
    }
  };

  return (
    <>
      <div ref={screenRenderer} />
      <div className="logo">
        <img width={150} alt="company logo" src={logo} />
      </div>
      <div className="controls">
        <p>Control Panel</p>
        <div>
          <label htmlFor="model-uploader">
            Upload Model (JSON file):
            <input
              name="model-uploader"
              onChange={fileUploaded}
              type="file"
              accept="application/JSON"
            />
          </label>
          {error.length > 3 && <p className="error-message">{error}</p>}
        </div>
        <button
          className="camera-controller"
          type="button"
          onClick={() => {
            changeCamera(currentCameraType === 'orthographic' ? 'perspective' : 'orthographic');
          }}
        >
          {currentCameraType === 'perspective' ? 'Draw Rectangle' : 'Perspective View'}
        </button>
        <button
          onClick={projectRectangle}
          className="camera-controller"
          type="button"
        >
          Project Rectangle
        </button>
        <div>
          <label htmlFor="draw-axis">
            Project rectangle-z coordinated:
            <input
              value={projctedRectangle?.position?.z}
              name="draw-axis"
              type="number"
              placeholder="z cordinate of projected plane"
              onChange={(e: any) => {
                changeRectanglesZ(e.target.value);
              }}
            />
          </label>
        </div>
      </div>
    </>
  );
}

export default App;
