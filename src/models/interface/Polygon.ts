import Points from 'models/types/Points';

interface Polygon {
    ['bounding_points']: Array<Points>
}

export default Polygon;
