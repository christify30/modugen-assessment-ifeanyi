import CreateSceneType from 'models/types/CreateScene';
// import cameraType from 'models/types/CameraType';
import changeCameraProjectionType from 'models/types/changeCameraProjection';

interface Scene {
    createScene(): CreateSceneType;
    changeCameraProjection(cameraType: string): changeCameraProjectionType
}

export default Scene;
