import CreateSceneType from 'models/types/CreateScene';
import Points from 'models/types/Points';

type DrawProps = {
    sceneProps: CreateSceneType,
    e: any,
    startPosition: Points,
    startMove: boolean,
    enableDrag: boolean
}

export default DrawProps;
