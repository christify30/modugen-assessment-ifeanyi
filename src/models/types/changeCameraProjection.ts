import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import cameraType from 'models/types/CameraType';

type changeCameraProjectionType = {
    camera: cameraType,
    controls: OrbitControls
}

export default changeCameraProjectionType;
