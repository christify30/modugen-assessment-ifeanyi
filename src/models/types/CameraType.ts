import * as THREE from 'three';

type cameraType = THREE.PerspectiveCamera | THREE.OrthographicCamera;
export default cameraType;
