import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import cameraType from 'models/types/CameraType';

 type CreateScene = {
    scene: THREE.Scene,
    controls: OrbitControls,
    renderer: THREE.WebGLRenderer,
    camera: cameraType
}

export default CreateScene;
